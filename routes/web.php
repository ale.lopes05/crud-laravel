<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ClienteController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/cliente', [ClienteController::class, 'index']);
Route::get('/clientes.create', [ClienteController::class, 'create']);
Route::post('/clientes.store', [ClienteController::class, 'store']);
Route::put('/clientes/update/{id}', [ClienteController::class, 'update']);
Route::delete('/clientes/destroy/{id}', [ClienteController::class, 'destroy']);
Route::get('/clientes/edit/{id}', [ClienteController::class, 'edit']);
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
