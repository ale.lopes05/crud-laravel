<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome', 50);
            $table->date('nascimento', 50);
            $table->string('email', 50);
            $table->string('cep', 50);
            $table->string('logradouro', 50);
            $table->string('uf', 50);
            $table->string('bairro', 50);
            $table->string('cidade', 50);
            $table->string('complemento', 50);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('clientes');
    }
}
