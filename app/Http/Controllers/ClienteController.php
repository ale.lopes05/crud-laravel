<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cliente;

class ClienteController extends Controller
{
    public function index() {
        $clientes = Cliente::all();
        $total = Cliente::all()->count();
        return view('list-clientes', compact('clientes', 'total'));
    }

    public function create(){
        return view('include-clientes');
    }

    public function store(Request $request){
        $cliente = new Cliente;
        $cliente->nome = $request->nome;
        $cliente->nascimento = $request->nascimento;
        $cliente->email = $request->email;
        $cliente->cep = $request->cep;
        $cliente->logradouro = $request->logradouro;
        $cliente->uf = $request->uf;
        $cliente->bairro = $request->bairro;
        $cliente->cidade = $request->cidade;
        $cliente->complemento = $request->complemento;
        $cliente->save();
        return redirect('/cliente')->with('message', 'Cliente registrado com sucesso');
    }

    public function show($id){
        //
    }

    public function edit($id){
        $cliente = Cliente::findOrfail($id);
        return view('alter-clientes', compact('cliente'));
    }

    public function update(Request $request, $id){
        $cliente = Cliente::findOrfail($id);
        $cliente->nome = $request->nome;
        $cliente->nascimento = $request->nascimento;
        $cliente->email = $request->email;
        $cliente->cep = $request->cep;
        $cliente->logradouro = $request->logradouro;
        $cliente->uf = $request->uf;
        $cliente->bairro = $request->bairro;
        $cliente->cidade = $request->cidade;
        $cliente->complemento = $request->complemento;
        $cliente->save();
        return redirect('/cliente')->with('message', 'Cliente alterado com sucesso');
    }

    public function destroy($id){
        $cliente = Cliente::findOrFail($id)->delete();
        //$cliente->delete();
        return redirect('/cliente')->with('message', 'Cliente excluído com sucesso'); 
    }
}
