<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $fillable = ['nome', 'nascimento', 'email', 'cep', 'logradouro', 'uf', 'bairro', 'cidade', 'complemento'];
    protected $guarded = ['id','created_at', 'update_at'];
    protected $table = 'clientes';
}
